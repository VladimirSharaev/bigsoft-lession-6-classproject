//
//  AddUserViewController.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionOfUser.h"
#import "User.h"

@protocol ViewControllerDelegate <NSObject>

- (void)addUser:(User *)user;

@end


@interface AddUserViewController : UIViewController

#pragma mark - Propertes

@property (nonatomic, weak) id <ViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;

#pragma mark - Actions

- (IBAction)addButtonWasPressed:(id)sender;

@end

//
//  PhoneNumber.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 25.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface PhoneNumber : NSObject <NSCoding>

#pragma mark - Properties

@property (nonatomic) NSString *phoneNumber;

#pragma mark - Methods

- (void)saveNumberToFile:(NSString *)ID user:(NSString *)userKey;
- (void)loadNumberFromFile:(NSString *)ID user:(NSString *)userKey;

@end

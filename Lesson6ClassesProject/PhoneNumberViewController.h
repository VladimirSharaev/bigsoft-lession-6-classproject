//
//  PhoneNumberViewController.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 25.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "AddPhoneNumberViewController.h"

@interface PhoneNumberViewController : UIViewController

#pragma mark - Propertes

@property (nonatomic) NSInteger userID;
@property (nonatomic) User *user;




@end

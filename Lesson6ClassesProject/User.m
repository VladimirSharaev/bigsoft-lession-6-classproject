//
//  User.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User.h"

@implementation User

#pragma mark - Constrictors

- (instancetype)initWithNameAndAge:(NSString *)name age:(NSNumber *)age
{
    self = [super init];
    if (self) {
        _age = age;
        _name = name;
        _arrayOfNumber = [NSMutableArray new];
    }
    return  self;
}

#pragma mark - NSCoder

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.name = [decoder decodeObjectForKey:@KEY_USER_NAME];
        self.age = [decoder decodeObjectForKey:@KEY_USER_AGE];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.name forKey:@KEY_USER_NAME];
    [encoder encodeObject:self.age forKey:@KEY_USER_AGE];
}

#pragma mark - Methods

- (NSString *)description
{
    return [NSString stringWithFormat:@"Name: %@ Age: %@", self.name, self.age ];
}

- (void)saveUserToFile:(NSString *)ID
{
    NSString *userKey = [NSString stringWithFormat:@"%s%@", KEY_USER, ID];
    [self saveCountOfNumberToFile:userKey];
    for (NSInteger i = 0; i < [self.countOfNumber integerValue]; i++) {
        PhoneNumber *phoneNumber = [self.arrayOfNumber objectAtIndex:i];
        NSString *ID = [NSString stringWithFormat:@"%ld",(long)(i + 1)];
        [phoneNumber saveNumberToFile:ID user:userKey];
    }
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:userKey];
    [userDefaults synchronize];
}

- (void)loadUserFromFile:(NSString *)ID
{
   NSString *userKey = [NSString stringWithFormat:@"%s%@", KEY_USER, ID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:userKey];
    User *user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    self.name = user.name;
    self.age = user.age;
    [self loadCountOfNumberFromFile:userKey];
    NSMutableArray *array = [NSMutableArray new];
    for (NSInteger i = 0; i < [self.countOfNumber integerValue]; i++) {
        NSString *ID = [NSString stringWithFormat:@"%ld",(long)(i + 1)];
        PhoneNumber *phoneNumber = [PhoneNumber new];
        [phoneNumber loadNumberFromFile:ID user:userKey];
        [array addObject:phoneNumber];
    }
    self.arrayOfNumber = array;
}

- (void)loadCountOfNumberFromFile:(NSString *)userKey
{
    NSString *keyCountOfNumber = [NSString stringWithFormat:@"%@%s", userKey, KEY_COUNT_OF_NUMBERS];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:keyCountOfNumber];
    NSNumber *countOfNumber = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (countOfNumber == nil) {
        self.countOfNumber = @(0);
    }else{
        self.countOfNumber = countOfNumber;
    }
}

- (void)saveCountOfNumberToFile:(NSString *)userKey
{
    NSString *keyCountOfNumber = [NSString stringWithFormat:@"%@%s", userKey, KEY_COUNT_OF_NUMBERS];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.countOfNumber];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:keyCountOfNumber];
    [userDefaults synchronize];
}

- (void)addPhoneNumber:(PhoneNumber *)phoneNumber userID:(NSString *)userID
{
    NSInteger count = [self.countOfNumber integerValue] + 1;
    self.countOfNumber = @(count);
    [self.arrayOfNumber addObject:phoneNumber];
    NSString *ID = [NSString stringWithFormat:@"%ld",(long)count];
    NSString *userKey = [NSString stringWithFormat:@"%s%@", KEY_USER, userID];
    [phoneNumber saveNumberToFile:ID user:userKey];
    [self saveCountOfNumberToFile:userKey];
}

@end

//
//  PhoneNumber.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 25.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "PhoneNumber.h"

@implementation PhoneNumber

#pragma mark - NSCoder

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.phoneNumber = [decoder decodeObjectForKey:@KEY_PHONE_NUMBER_PROPERTY];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.phoneNumber forKey:@KEY_PHONE_NUMBER_PROPERTY];
}

#pragma mark - Methods

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", self.phoneNumber];
}

- (void)saveNumberToFile:(NSString *)ID user:(NSString *)userKey
{
    NSString *numberKey = [NSString stringWithFormat:@"%@%s%@", userKey, KEY_NUMBER, ID];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:numberKey];
    [userDefaults synchronize];
}

- (void)loadNumberFromFile:(NSString *)ID user:(NSString *)userKey
{
    NSString *numberKey = [NSString stringWithFormat:@"%@%s%@", userKey, KEY_NUMBER, ID];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:numberKey];
    PhoneNumber *phoneNumber = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    self.phoneNumber = phoneNumber.phoneNumber;
}

@end

//
//  CollectionOfUser.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "User.h"

@interface CollectionOfUser : NSObject

#pragma mark - Properties

@property (nonatomic) NSNumber *countOfUser;
@property (nonatomic) NSMutableArray *arrayOfUsers;


#pragma mark - Methods

- (void)loadCountOfUserFromFile;
- (void)saveCountOfUserToFile;
- (void)loadAllUsersFromFile;
- (void)saveAllUserToFile;
- (void)addUser:(User *)user;


@end

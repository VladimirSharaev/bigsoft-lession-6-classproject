//
//  PhoneNumberViewController.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 25.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "PhoneNumberViewController.h"

@interface PhoneNumberViewController () <UITableViewDataSource, UITableViewDelegate, PhoneNumberDelegate>

@property (nonatomic) UITableView *tableview;

@end

@implementation PhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableview = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    [self.view addSubview:self.tableview];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return self.user.arrayOfNumber.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
        cell.textLabel.text = @"Add new phone number";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",((PhoneNumber *)self.user.arrayOfNumber[indexPath.row])];
        return cell;
    }
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44.0f;
    } else {
        return 30.0f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
    if (indexPath.section == 0) {
        AddPhoneNumberViewController *addPhoneNumberViewController = [AddPhoneNumberViewController new];
        UIStoryboard *storyboard = self.storyboard;
        addPhoneNumberViewController = [storyboard instantiateViewControllerWithIdentifier:@"AddPhoneNumberViewController"];
        addPhoneNumberViewController.delegate = self;
        [self.navigationController pushViewController:addPhoneNumberViewController animated:YES];
    }
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Phone number Delegate

- (void)addNumber:(PhoneNumber *)phoneNumber
{
    NSString *userID = [NSString stringWithFormat:@"%ld", (long)self.userID];
    [self.user addPhoneNumber:phoneNumber userID:userID];
    [self.tableview reloadData];
}

@end

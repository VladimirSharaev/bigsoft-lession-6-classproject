//
//  AddPhoneNumberViewController.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 25.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneNumber.h"

@protocol PhoneNumberDelegate <NSObject>

- (void)addNumber:(PhoneNumber *)phoneNumber;

@end


@interface AddPhoneNumberViewController : UIViewController

#pragma mark - Propertes

@property (nonatomic, weak) id <PhoneNumberDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *numberTextField;

#pragma mark - Actions

- (IBAction)addButtonWasPressed:(id)sender;

@end

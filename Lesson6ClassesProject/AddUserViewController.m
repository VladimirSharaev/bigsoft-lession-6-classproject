//
//  AddUserViewController.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AddUserViewController.h"

@interface AddUserViewController ()

@end

@implementation AddUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)addButtonWasPressed:(id)sender
{
    NSString *name = self.nameTextField.text;
    NSNumber *age = [NSNumber numberWithInteger:[self.ageTextField.text integerValue]];
    if ((![self.nameTextField.text  isEqual: @""]) && (![self.ageTextField.text  isEqual: @""])) {
        User *user = [[User alloc] initWithNameAndAge:name age:age];
        if (self.delegate) {
            [self.delegate addUser:user];
        }
        self.nameTextField.text = @"";
        self.ageTextField.text = @"";
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter correct date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

@end

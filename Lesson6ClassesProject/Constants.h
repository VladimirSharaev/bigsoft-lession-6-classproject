//
//  Constants.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#ifndef Lesson6ClassesProject_Constants_h
#define Lesson6ClassesProject_Constants_h

#define KEY_COUNT_OF_USER "NUMBER_OF_USER"
#define KEY_COUNT_OF_NUMBERS "NUMBER_OF_NUMBERS"
#define KEY_USER "USER"
#define KEY_USER_NAME "USER_NAME"
#define KEY_USER_AGE "USER_AGE"
#define KEY_NUMBER "NUMBER"
#define KEY_PHONE_NUMBER_PROPERTY "PHONE_NUMBER"

#endif

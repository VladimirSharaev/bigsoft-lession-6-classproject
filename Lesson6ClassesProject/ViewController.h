//
//  ViewController.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddUserViewController.h"
#import "PhoneNumberViewController.h"
#import "CollectionOfUser.h"
#import "User.h"

@interface ViewController : UIViewController

@property (nonatomic) CollectionOfUser *collectionOfUser;

@end


//
//  ViewController.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "ViewController.h"
#import "User.h"
#import "CollectionOfUser.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, ViewControllerDelegate>

@property (nonatomic) UITableView *tableview;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    self.collectionOfUser = [CollectionOfUser new];
    
    [self.collectionOfUser loadCountOfUserFromFile];
    [self.collectionOfUser loadAllUsersFromFile];
    
    self.tableview = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    [self.view addSubview:self.tableview];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return self.collectionOfUser.arrayOfUsers.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
        cell.textLabel.text = @"Add new user";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",((User *)self.collectionOfUser.arrayOfUsers[indexPath.row])];
        return cell;
    }
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44.0f;
    } else {
        return 30.0f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSelector:@selector(deselectRowAtIndexPath:) withObject:indexPath afterDelay:0.3];
    if (indexPath.section == 0) {
        AddUserViewController *addUserViewController = [AddUserViewController new];
        UIStoryboard *storyboard = self.storyboard;
        addUserViewController = [storyboard instantiateViewControllerWithIdentifier:@"AddUserViewController"];
        addUserViewController.delegate = self;
        [self.navigationController pushViewController:addUserViewController animated:YES];
    }else{
        PhoneNumberViewController *phoneNumberViewController = [PhoneNumberViewController new];
        UIStoryboard *storyboard = self.storyboard;
        phoneNumberViewController = [storyboard instantiateViewControllerWithIdentifier:@"PhoneNumberViewController"];
        phoneNumberViewController.userID = indexPath.row + 1;
        phoneNumberViewController.user = (User *)self.collectionOfUser.arrayOfUsers[indexPath.row];
        [self.navigationController pushViewController:phoneNumberViewController animated:YES];

    }
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - View Controller Delegate

- (void)addUser:(User *)user
{
    [self.collectionOfUser addUser:user];
    [self.tableview reloadData];
}

#pragma mark - Actions


@end

//
//  CollectionOfUser.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "CollectionOfUser.h"

@implementation CollectionOfUser

#pragma mark - Constryctors

- (instancetype)init
{
    self = [super init];
    if (self) {
        _countOfUser = @(0);
        _arrayOfUsers = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Methods

- (void)saveCountOfUserToFile
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.countOfUser];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:data forKey:@KEY_COUNT_OF_USER];
    [userDefaults synchronize];
}

- (void)loadCountOfUserFromFile
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:@KEY_COUNT_OF_USER];
    NSNumber *countOfUsers = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (countOfUsers == nil) {
        self.countOfUser = @(0);
    }else{
        self.countOfUser = countOfUsers;
    }
}

- (void)saveAllUserToFile
{
    for (NSInteger i = 0; i < [self.countOfUser integerValue]; i++) {
        User *user = [self.arrayOfUsers objectAtIndex:i];
        NSString *ID = [NSString stringWithFormat:@"%ld",(long)(i + 1)];
        [user saveUserToFile:ID];
    }
}

- (void)loadAllUsersFromFile
{
    NSMutableArray *array = [NSMutableArray new];
    for (NSInteger i = 0; i < [self.countOfUser integerValue]; i++) {
        NSString *ID = [NSString stringWithFormat:@"%ld",(long)(i + 1)];
        User *user = [User new];
        [user loadUserFromFile:ID];
        [array addObject:user];
    }
    self.arrayOfUsers = array;
}

- (void)addUser:(User *)user
{
    NSInteger count = [self.countOfUser integerValue] + 1;
    self.countOfUser = @(count);
    [self.arrayOfUsers addObject:user];
    NSString *ID = [NSString stringWithFormat:@"%ld",(long)count];
    [user saveUserToFile:ID];
    [self saveCountOfUserToFile];
}

@end

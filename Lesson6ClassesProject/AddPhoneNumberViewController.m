//
//  AddPhoneNumberViewController.m
//  Lesson6ClassesProject
//
//  Created by Vladimir on 25.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AddPhoneNumberViewController.h"

@implementation AddPhoneNumberViewController

- (IBAction)addButtonWasPressed:(id)sender
{
    NSString *number = self.numberTextField.text;
    if (![self.numberTextField.text  isEqual: @""]) {
        PhoneNumber *phoneNumber = [PhoneNumber new];
        phoneNumber.phoneNumber = number;
        if (self.delegate) {
            [self.delegate addNumber:phoneNumber];
        }
        self.numberTextField.text = @"";
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter correct date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}
@end

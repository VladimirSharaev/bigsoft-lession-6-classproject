//
//  User.h
//  Lesson6ClassesProject
//
//  Created by Vladimir on 19.04.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "PhoneNumber.h"

@interface User : NSObject <NSCoding>

#pragma mark - Constructors

- (instancetype)initWithNameAndAge:(NSString *)name age:(NSNumber *)age;

#pragma mark - Properties

@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *age;
@property (nonatomic) NSNumber *countOfNumber;
@property (nonatomic) NSMutableArray *arrayOfNumber;

#pragma mark - Methods

- (void)saveUserToFile:(NSString *)ID;
- (void)loadUserFromFile:(NSString *)ID;
- (void)loadCountOfNumberFromFile:(NSString *)userKey;
- (void)saveCountOfNumberToFile:(NSString *)userKey;
- (void)addPhoneNumber:(PhoneNumber *)phoneNumber userID:(NSString *)userID;


@end
